
/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

/*Using promises and the `fetch` library, do the following. 
If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
Usage of the path libary is recommended

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
*/

let path = require('path')
let fs = require('fs');


// 1. Fetch all the users

function fetchUsers(api) {
    return fetch(api)
        .then((data) => {
            if (data.ok) {
                return (data.json());

            } else {
                throw new Error(`${data.status}`)
            }
        }).then((data) => {

            return data
        }).then((data) => {
            fs.writeFile(path.join(__dirname, 'userData1.json'), JSON.stringify(data), (err, data) => {
                if (err) {
                    reject(new Error('err writing files'), err)

                } else {
                    console.log('files written');
                }
            })
        }).catch((err) => {
            console.log(err);
        })
}

// 2. Fetch all the todos

function fetchTodos(api) {
    return fetch(api)
        .then((data) => {
            if (data.ok) {
                return (data.json());

            } else {
                throw new Error(`${data.status}`)
            }
        }).then((data) => {

            return data
        }).then((data) => {
            fs.writeFile(path.join(__dirname, 'todoData1.json'), JSON.stringify(data), (err, data) => {
                if (err) {
                    err = new Error('err writing files')
                    console.log(err);
                } else {
                    console.log('files written');
                }
            })
        }).catch((err) => {
            console.log(err);
        })
}

// 3. Use the promise chain and fetch the users first and then the todos.

function fetchAllData(api1, api2) {
    return new Promise(() => {
        return fetchUsers(api1)
    }).then(() => {
        return fetchTodos(api2)
    }).catch((err) => {
        throw new Error(err)
    })
}

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function getUserData(api) {

    return fetch(api)
        .then((data) => {
            if (data.ok) {

                return (data.json());

            } else {
                throw new Error(`${data.status}`)
            }
        }).then((data) => {
            data.map((dataObj) => {
                fetch(`https://jsonplaceholder.typicode.com/users?id=${dataObj['id']}`).then((data) => {
                    return data.json()
                }).then((data) => {
                    fs.writeFile(path.join(__dirname, 'userIdData.json'), JSON.stringify(data), (err, data) => {
                        if (err) {
                            reject(err)
                        } else {
                            console.log('files written to userIdData.json');
                        }
                    })
                })
            })
        }).catch((err) => {
            throw new Error(err)
        })

}
